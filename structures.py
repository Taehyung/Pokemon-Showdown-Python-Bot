from tools import toID
from collections import deque

class Room:
    def __init__(self, initstring):
        initlist = initstring.split('\n')
        self.id = toID(initlist[0])
        self.type = initlist[1].split('|')[2]
        self.title = initlist[2].split('|')[2]
        self.userlist = set(initlist[3].split('|')[2].split(',')[1:])
        #self.log = deque(initlist[5:])
        
    """
    def updatelog(self, message):
        self.log.append(message)
        if(len(self.log)>250):
            self.log.popleft()"""
    def updateul(self, opts):
        opts = opts.split('|')
        if opts[1] == 'J':
            self.userlist.add(opts[2])
        elif opts[1] == 'L':
            for u in self.userlist:
                if opts[2] == toID(u):
                    self.userlist.discard(u)
                    break
        elif opts[1] == 'N':
            for u in self.userlist:
                if opts[3] == toID(u):
                    self.userlist.discard(u)
                    break
            self.userlist.add(opts[2])
