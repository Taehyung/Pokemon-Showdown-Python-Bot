import Bot
import tools
import structures

bot = Bot.Bot()

rdict = {}

i = 1
while bot.ws.connected:
    i+=1
    resp = bot.recv()
    room = ''
    r_l = resp.split('\n')
    if len(r_l)!= 1:
        room = r_l[0][1:]
        opts = r_l[1].split('|')
    else:
        opts = r_l[-1].split('|')
    if len(opts) == 1:
        continue
    elif opts[1]=='updateuser':
        continue
    elif opts[1]=='init':
        rdict[room] = structures.Room(resp)
    elif opts[1]=='J':
        rdict[room].updateul(r_l[1])
    elif opts[1]=='L':
        rdict[room].updateul(r_l[1])
    elif opts[1]=='N':
        rdict[room].updateul(r_l[1])
    elif opts[1]=='c:':
        if opts[4][0] in bot.comchar:
            com = opts[4][1:].split(' ')[0]
            arg = ' '.join(opts[4][1:].split(' ')[1:])
            bot.commands.com(bot, room, arg, opts[3], com)
    elif opts[1]=='pm':
        if opts[4][0] in bot.comchar:
            com = opts[4][1:].split(' ')[0]
            arg = ' '.join(opts[4][1:].split(' ')[1:])
            bot.commands.com(bot, room, arg, opts[2], com, True)
