from tools import toID
from importlib import reload
import time, os
import commands

def dc(bot, room, text, user, pm):
    bot.dc()

def c(bot, room, text, user, pm):
    if pm:
        if text[0] == '[' and ']' in text:
            e = text.index(']')
            room = text[1:e]
            text = text[e+1:].strip()
        else:
            text = '/w '+user+','+text
    bot.send(room, text)

def restart(bot, room, text, user, pm):
    bot.dc()
    os.system('python main.py')

def contime(bot, room, text, user, pm):
    pmtext = ''
    if pm:
        pmtext = '/pm '+user+','
    daytext = ''; hourtext = ''; minutetext = ''; secondtext = ''
    td = time.time() - bot.contime
    days = int(td/(24*60*60));  td-=days*24*60*60
    hours = int(td/(60*60)); td-=hours*60*60
    minutes = int(td/(60)); td-=minutes*60
    seconds = round(td, 2)
    if days!=0:
        daytext = str(days)+' days'
    if hours!=0:
        hourtext = str(hours)+' hours'
    if minutes!=0:
        minutetext = str(minutes)+' minutes'
    if seconds != 0:
        secondtext = str(seconds)+' seconds'
    bot.send(room, pmtext+'The bot has been connected for: '+(daytext+' '+hourtext+' '+minutetext+' '+secondtext).strip()+'.')

def repo(bot, room, text, user, pm):
    pmtext = ''
    if pm:
        pmtext = '/pm '+user+','
    bot.send(room, pmtext+"https://gitlab.com/XpRienzo/Pokemon-Showdown-Python-Bot")

def hotpatch(bot, room, text, user, pm):
    #to be extended to include config, current implementation just a dummy.
    bot.commands = reload(commands)
    bot.send(room, 'The commands have been hotpatched!')

fdict = {'dc':dc, 'custom':c, 'c':c, 'contime':contime, 'uptime':contime, 'kill':dc, 'restart':restart, 'hotpatch':hotpatch, 'reload':hotpatch, 'git':repo, 'repo':repo}

devcoms = ['dc', 'custom', 'c', 'kill', 'restart', 'hotpatch', 'reload']

infocoms = ['uptime', 'contime', 'git', 'repo']

ranks = {'uptime':'+', 'contime':'+', 'git':'+', 'repo':'+'}

def com(bot, room, text, user, command, pm=False):
    if toID(user) == toID(bot.usr['nickname']):
        return
    if command in devcoms and toID(user) in bot.devs:
        fdict[command](bot, room, text, user, pm)
        return
    if command in infocoms:
        prio = bot.ranks.index(ranks[command])
        if prio > bot.ranks.index(user[0]):
            fdict[command](bot, room, text, user, True)
        else:
            fdict[command](bot, room, text, user, pm)
