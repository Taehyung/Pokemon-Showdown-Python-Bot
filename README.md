# Pokemon Showdown Python Bot

A simple WIP bot written in Python 3.7 for [Pokémon Showdown](https://github.com/Zarel/Pokemon-Showdown).

## Quick Set-Up

- Edit `config-example.py` to your needs and rename it to `config.py`.

- Install necessary packages by running `pip install -r requirements.txt`

- Run `python main.py` (substitute python with python3 if it's needed)

## License

This project is distributed under the terms of the [MIT License](https://gitlab.com/XpRienzo/Pokemon-Showdown-Python-Bot/blob/master/LICENSE)

## Credits

- XpRienzo (Owner, Developer)